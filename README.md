# Scrap Partes Diarios ENARGAS

Permite tomar la información de los partes diarios de ENARGAS, determinando un rango de fechas. El script deja disponible el PDF original y el extracto de la tabla en CSV para su utilización.


## Variables

Dentro del script se debe establecer el rango a realizar el scrapeo:

`start_date` -> Fecha de inicio

`end_date` -> Fecha final


## Links

 - [Origenes de datos](https://www.enargas.gob.ar/secciones/transporte-y-distribucion/partes-diarios.php)


## Instalación

Debe tener instalado previamente JAVA, ya que es utilizada por la librería "tabula".

```bash
  python -m venv venv
  venv/Scripts/activate
  pip install -r requirements.txt
  python main.py
```
    