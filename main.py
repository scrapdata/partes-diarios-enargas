import requests
import tabula
import pandas as pd
from datetime import date, timedelta


global data
start_date = date(2023, 1, 1)
end_date = date(2023, 1, 2)
tipo = 'real'
path = 'partes-diarios/real'

# Descarga automatica de partes diarios ENARGAS de un rango determinado
# https://www.enargas.gob.ar/secciones/transporte-y-distribucion/partes-diarios.php

def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)

def download_file(PARAMS):
    global data
    url = "https://www.enargas.gob.ar/secciones/transporte-y-distribucion/descarga.php"
    file = requests.get(url, params = PARAMS)
    data = file

def save_file(name):
    global data
    filename = f"data/{name}.pdf"
    line_to_delete = 4
    initial_line = 1
    file_lines = {}
    
    with open( filename, 'wb') as f:
        f.write(data.content) 
    
    #Limpia las primeras 4 lineas PHP generadas    
    l1 = []
    with open(filename, 'rb') as fp:
        l1 = fp.readlines()
    
    with open(filename, 'wb') as fp:
        for number, line in enumerate(l1):
            if number not in range(0,4):
             fp.write(line)      
         
    del data

def pdf2csv(periodo):
    _COLUMNS = ['distribuidoras_cargadores_directos', 'cap_contratada_sm3dia_x1000', 'vol_req_a_transco_sm3dia_x1000', 'vol_auth_por_transco_sm3dia_x1000', 'cons_real_sm3dia_x1000']
 
    table = tabula.read_pdf(f"data/{periodo}.pdf",pages=1)
    table_gasoductos = table[0].iloc[8:]
    table_gasoductos = table_gasoductos.reset_index(drop=True)
    table_gasoductos.columns = _COLUMNS

    table_gasoductos_prop = table[1].iloc[:1]
    table_gasoductos_prop = table_gasoductos_prop.reset_index(drop=True)
    table_gasoductos_prop.columns = _COLUMNS

    table_total = pd.concat([table_gasoductos, table_gasoductos_prop], axis=0)
    table_total.to_csv(f"data/{periodo}.csv", encoding='utf-8', index=False)
    
#Loop rango de fecha definido   
for single_date in daterange(start_date, end_date):
    print("Descargando: " + single_date.strftime("%Y-%m-%d"))    
    periodo = single_date.strftime("%Y%m%d")
    download_file({'tipo': tipo,'path': path,'file': periodo + '.pdf'})  
    save_file(periodo) 
    pdf2csv(periodo)